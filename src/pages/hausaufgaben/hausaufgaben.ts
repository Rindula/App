import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-hausaufgaben',
  templateUrl: 'hausaufgaben.html'
})
export class HausaufgabenPage {

  aufgabenArray: any;
  posts: any;

  constructor(public navCtrl: NavController, public http: Http, private alertCtrl: AlertController) {
    this.posts = null;
    this.http.get('https://apis.rindula.de/schule/fetch.php?t=hausaufgaben').subscribe(
      data => {
        this.posts = data.json().data;
      }, err => {
        console.error("##### FEHLER #####");
        console.error(err);
        console.error("##################");
      }
    )
  

  }

  presentAlert(fach: String, datum: String, aufgaben: String) {
    var a = aufgaben.split(";");
    var afg = "";
    a.forEach(e => {
      afg += "<li>" + e + "</li>"
    });
    let alert = this.alertCtrl.create({
      title: fach + "",
      subTitle: "<small>" + datum + "</small><br><ul>" + afg + "</ul>",
      buttons: ['Schließen']
    });
    alert.present();
  }

}
